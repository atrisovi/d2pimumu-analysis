# Analysis case study

This is an analysis case study that illustrates the main steps of LHCb physics analyses.

# Decay modes

```
D+ -> pi+ mu+ mu-
```

# Data

`LHCb Collision 11 Beam3500GeV-VeloClosed-MagDownRealData Reco14 Stripping21r1 90000000 CHARMMDST`

`LHCb Collision 12 Beam4000GeV-VeloClosed-MagDownRealData Reco14 Stripping21 90000000 CHARMMDST`

# Environment

`source  /cvmfs/lhcb.cern.ch/lib/LbLogin.sh --no-userarea`

# Location

`data` and `monte_carlo`

# ANA Note

`https://cds.cern.ch/record/1421263/files/LHCb-ANA-2012-025.pdf`
